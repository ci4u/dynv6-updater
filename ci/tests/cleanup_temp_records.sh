#!/bin/sh
set -eu

apk add curl findutils jq
wget -q -O /usr/local/bin/yq https://github.com/mikefarah/yq/releases/download/v4.13.2/yq_linux_amd64
chmod +x /usr/local/bin/yq
yq --version
jq --version
curl --version

get_zone_id(){
    NAME=${1}
    TOKEN=${2}
    ZONE_INFO=$(curl -s \
    -H "Authorization: Bearer ${TOKEN}" \
    -H "Accept: application/json" \
    https://dynv6.com/api/v2/zones)
    RESPONSE_NAME=$(echo ${ZONE_INFO} | jq -r '.[0].name')
    if ! echo ${RESPONSE_NAME} | grep -E "^${NAME}$" > /dev/null; then
        echo "expected zone name '${NAME}' but API returned '${RESPONSE_NAME}'";
        # exit 1
    fi
    echo ${ZONE_INFO} | jq -r '.[0].id'
}

get_zone_records(){
    ZONE_ID=${1}
    ZONE_TOKEN=${2}
    ZONE_RECORDS=$(curl -s \
    -H "Authorization: Bearer ${ZONE_TOKEN}" \
    -H "Accept: application/json" \
    "https://dynv6.com/api/v2/zones/{${ZONE_ID}}/records")
    echo ${ZONE_RECORDS}
}

COUNTER=0

while true
do
    ZONE_NAME=$(yq e ".dns_zones[${COUNTER}].name" ./secrets.yaml)
    ZONE_TOKEN=$(yq e ".dns_zones[${COUNTER}].token" ./secrets.yaml)

    if [ "${ZONE_NAME}" = "null" ]; then
        echo "No more zones left."
        exit 0
    fi

    grep -E "${ZONE_NAME}$" dns_entries.txt | while read -r line ; do
        echo "Deleting entry '${line}'"
        ZONE_ID=$(get_zone_id ${ZONE_NAME} ${ZONE_TOKEN})
        ZONE_ENTRIES=$(get_zone_records ${ZONE_ID} ${ZONE_TOKEN})
        # filter temp entries
        FILTERED_ENTRIES=$(echo "${ZONE_ENTRIES}" | jq -c ".[] | select(.name | contains(\"${CI_JOB_ID}\"))")
        # delete entries by id
        echo ${FILTERED_ENTRIES} | jq -r ".id" | xargs -L1 -I'{}' curl -s -X DELETE \
            -H "Authorization: Bearer ${ZONE_TOKEN}" \
            -H "Accept: application/json" \
            https://dynv6.com/api/v2/zones/${ZONE_ID}/records/{}
    done
    COUNTER=$((COUNTER + 1))
done
