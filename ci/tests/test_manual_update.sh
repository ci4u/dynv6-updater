#!/bin/sh
set -eu
source ./ci/tests/utils/functions.sh

# set manual IPs
RANDOM_NUMBER=$(shuf -i 1-99 -n 1)
export MANUAL_IPV4=0.0.0.${RANDOM_NUMBER}
echo "Manual IPv4 is ${MANUAL_IPV4}"
export MANUAL_IPV6=1::${RANDOM_NUMBER}
echo "Manual IPv6 is ${MANUAL_IPV6}"

# run compose up
docker-compose -f ./ci/tests/manifests/manual-ipv4+ipv6.yml --project-directory ${PWD} down
docker-compose -f ./ci/tests/manifests/manual-ipv4+ipv6.yml --project-directory ${PWD} up

# resolve records
while read line; do
    if [ ! -z "${line}" ]; then
        validate_ipv4_record ${line} ${MANUAL_IPV4}
        validate_ipv6_record ${line} ${MANUAL_IPV6}
    fi
done < ./dns_entries.txt
