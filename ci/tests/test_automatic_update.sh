#!/bin/sh
set -eu
source ./ci/tests/utils/functions.sh

# get public IP
IPV4_ADDRESS=$(wget -q -4 -O- https://api.ipify.org)
echo "Public IPv4 is ${IPV4_ADDRESS}"

# run compose up
docker-compose -f ./ci/tests/manifests/automatic-ipv4.yml --project-directory ${PWD} down
docker-compose -f ./ci/tests/manifests/automatic-ipv4.yml --project-directory ${PWD} up

# resolve records
while read line; do
    if [ ! -z "${line}" ]; then
        validate_ipv4_record ${line} ${IPV4_ADDRESS}
    fi
done < ./dns_entries.txt
