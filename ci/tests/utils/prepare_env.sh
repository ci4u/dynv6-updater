#!/bin/sh
set -eu

# prepare dns_entries.txt
cat <<EOF | tee ./dns_entries.txt
test1-${CI_JOB_ID}.gitlab.v6.rocks
test2-${CI_JOB_ID}.gitlab.v6.rocks
test3-${CI_JOB_ID}.special.gitlab.v6.rocks
test4-${CI_JOB_ID}.other.gitlab.v6.rocks
test5-${CI_JOB_ID}.gitlab2.v6.rocks
test6-${CI_JOB_ID}.gitlab2.v6.rocks
test7-${CI_JOB_ID}.special.gitlab2.v6.rocks
test8-${CI_JOB_ID}.other.gitlab2.v6.rocks
EOF

# prepare secrets.yaml
cat <<EOF | tee ./secrets.yaml
dns_zones:
  - name: gitlab.v6.rocks
    token: ${TOKEN_GITLAB_V6_ROCKS}
  - name: gitlab2.v6.rocks
    token: ${TOKEN_GITLAB2_V6_ROCKS}
EOF
