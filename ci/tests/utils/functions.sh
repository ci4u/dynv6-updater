#!/bin/sh
set -eu

DNS_UPDATE_TIMEOUT=120

validate_ipv4_record() {
    # 1=subdomain.example.org
    # 2=1.2.3.4
    COUNTER=0
    IP_4=""
    while [ -z "${IP_4}" ] && [ ${COUNTER} -lt ${DNS_UPDATE_TIMEOUT} ]; do
        IP_4=$(nslookup -4 ${1} 1.1.1.1 | grep ${2}) || sleep 1
        COUNTER=$((COUNTER + 1))
    done
    if [ -z "${IP_4}" ]; then
        echo "${1} IPv4 timed out"
        exit 1
    else
        echo "A record '${1}' validated"
    fi
}

validate_ipv6_record() {
    # 1=subdomain.example.org
    # 2=1::1
    COUNTER=0
    IP_6=""
    while [ -z "${IP_6}" ] && [ ${COUNTER} -lt ${DNS_UPDATE_TIMEOUT} ]; do
        IP_6=$(nslookup -6 ${1} 1.1.1.1 | grep ${2}) || sleep 1
        COUNTER=$((COUNTER + 1))
    done
    if [ -z "${IP_6}" ]; then
        echo "${1} IPv4 timed out"
        exit 1
    else
        echo "AAAA record '${1}' validated"
    fi
}
