#!/bin/sh
set -eu

if docker pull "${CI_REGISTRY_IMAGE}:_TEMP_${CI_COMMIT_SHORT_SHA}"; then
    echo "successfully pulled cached image"
else
    echo "no cached image available"
fi

docker-buildx create --use

docker-buildx build \
    --platform linux/amd64,linux/arm/v7 \
    --tag ${1} ${2:''} .
