#!/bin/sh
set -eu

apk add --no-cache bind-tools wget
echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY \
    --username $CI_REGISTRY_USER --password-stdin
wget -q -O /usr/local/bin/docker-buildx \
    "https://github.com/docker/buildx/releases/download/${BUILDX_VERSION}/buildx-${BUILDX_VERSION}.${BUILDX_ARCH}"
chmod +x /usr/local/bin/docker-buildx
