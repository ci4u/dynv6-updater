#!/bin/sh
set -eu

apk add git openssh-client

eval $(ssh-agent -s)
echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add -

mkdir -p ~/.ssh
chmod 700 ~/.ssh

ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

git config --global user.email "pipeline@gitlab.com"
git config --global user.name "Gitlab CI"

ssh -vvv git@gitlab.com

echo "git remotes:"
git remote -v
echo "deleting origin"
git remote rm origin
echo "adding new origin:"
git remote add origin "git@gitlab.com:${CI_PROJECT_PATH}.git"
echo "git remotes:"
git remote -v

# try it up to 5 times
for i in {1..5};
do
    git pull origin master --tags

    LATEST_TAG=$(git describe --tags $(git rev-list --tags --max-count=1))
    LATEST_BUILD_TAG=$(echo "${LATEST_TAG}" | rev | cut -d'.' -f1 | rev)
    LATEST_BUILD_TAG_PREFIX=$(echo "${LATEST_TAG}" | rev | cut -d'.' -f2- | rev)

    if git log -n 1 --decorate --pretty=oneline | grep -E "tag: ([0-9]+.)+[0-9]+"; then
        echo "There is already a tag on the current commit"
        exit 0
    else
        echo "Creating a new build tag for the current commit"
        NEW_BUILD_TAG=$((LATEST_BUILD_TAG + 1))
        NEW_TAG="${LATEST_BUILD_TAG_PREFIX}.${NEW_BUILD_TAG}"
        git tag "${NEW_TAG}" -m "${CI_COMMIT_SHORT_SHA}"
        git push origin "${NEW_TAG}" 2>&1 && echo "push succeeded" && exit 0 ||\
        echo "retrying.."
    fi
done
echo "push failed after 5 attempts"
exit 1
