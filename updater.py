"""
script to update all domains in dns_entries.txt to your public IP
creates missing domains automatically
supports IPv4 and IPv6
"""

#!/bin/env python3

import json
import ipaddress
import logging
import requests
import os
from pprint import pprint
import yaml
import socket
import requests.packages.urllib3.util.connection as urllib3_cn

API_URL = 'https://dynv6.com/api/v2'
API_TOKEN = None
DNS_ENTRIES_FILENAME = "dns_entries.txt"
SECRETES_YAML_FILE = "secrets.yaml"

LOG_LEVEL = os.getenv('LOG_LEVEL', "INFO")
ENABLE_IPV4 = os.getenv('ENABLE_IPV4', False)
ENABLE_IPV6 = os.getenv('ENABLE_IPV6', False)
MANUAL_IPV4 = os.getenv('MANUAL_IPV4', None)
MANUAL_IPV6 = os.getenv('MANUAL_IPV6', None)

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.ERROR
)

logger = logging.getLogger("dynv6-updater")

logger.setLevel(LOG_LEVEL)

if not ENABLE_IPV4 and not ENABLE_IPV6:
    logger.error(
        "error: neither ENABLE_IPV4 nor ENABLE_IPV6 were enabled, nothing to do.")
    exit(1)

with open(SECRETES_YAML_FILE, "r") as stream:
    try:
        config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

dns_zones = config.get('dns_zones')


def api_generic(uri, data, method):
    if API_TOKEN is None:
        logger.error("API_TOKEN was not set")
        exit(1)
    url = "%s%s" % (API_URL, uri)
    headers = {'Content-Type': 'application/json',
               'Authorization': 'Bearer %s' % API_TOKEN}
    raw_response = method(url, headers=headers, json=data)
    return raw_response


def decode_binary_json(binary_json_string):
    decoded_response = binary_json_string.content.decode("utf8")
    response_dict = json.loads(decoded_response)
    return response_dict


def api_post(uri, data):
    raw_response = api_generic(uri, data, requests.post)
    response_dict = decode_binary_json(raw_response)
    return response_dict


def api_patch(uri, data):
    raw_response = api_generic(uri, data, requests.patch)
    response_dict = decode_binary_json(raw_response)
    return response_dict


def api_get(uri):
    raw_response = api_generic(uri, None, requests.get)
    response_dict = decode_binary_json(raw_response)
    return response_dict


def api_delete(uri):
    raw_response = api_generic(uri, None, requests.delete)
    if raw_response.status_code == 204:
        return "successfully deleted"
    else:
        return raw_response.status_code


def get_zone_id_name(zone):
    zone_name = zone.get('name')
    global API_TOKEN
    API_TOKEN = zone.get('token')
    zone_query = api_get("/zones")
    if len(zone_query) == 1:
        api_zone_name = zone_query[0].get('name')
        if api_zone_name == zone_name:
            logger.debug("zone '%s' is valid", zone_name)
        else:
            logger.error(
                "API did not retrieve zone_name '%s', but '%s'", zone_name, api_zone_name)
            exit(1)
        zone_id = zone_query[0].get('id')
    elif len(zone_query) > 1:
        logger.error("API returned more than one zone: '%s'", zone_query)
        exit(1)
    else:
        logger.error("API returned no zone for zone_name '%s'", zone_name)
        exit(1)
    return zone_id, zone_name


def read_file_to_list(filename):
    file_handle = open(filename, "r")
    # read file, trim spaces
    file_content = file_handle.read().strip()
    lines = file_content.split('\n')
    return lines


def get_zone_records(zone_id):
    records = api_get("/zones/%s/records" % zone_id)
    return records


def get_missing_strings(desired, existing):
    # non empty missing strings
    return [s for s in desired if not s in existing]


def create_missing_generic_entries(zone_name, zone_id, records_short, entry_content, entry_type):
    if len(records_short) == 0:
        logger.info("no missing records")
    data = {"data": entry_content, "type": entry_type}
    new_records = []
    # create them
    for record_name in records_short:
        data["name"] = record_name
        new_record = api_post("/zones/%s/records" % zone_id, data=data)
        if new_record.get('name', None):
            new_records.append(new_record)
            logger.info("%s '%s' created", entry_type, record_name)
    return new_records


def update_entries(zone_id, zone_name, entries, ip_address, entry_type):
    for entry in entries:
        entry_id = entry.get('id')
        entry_name = entry.get('name')
        entry_value = entry.get('data')

        if entry_value == ip_address:
            logger.info("%s '%s.%s' already up to date",
                        entry_type, entry_name, zone_name)
            continue
        else:
            logger.debug("entry_value: %s", entry_value)
            logger.debug("ip_address:  %s", ip_address)

        payload = {
            "data": ip_address,
        }
        update_response = api_patch(
            "/zones/%s/records/%s" % (zone_id, entry_id), payload)
        new_data = update_response.get('data', None)
        if new_data == ip_address:
            logger.info("%s '%s.%s' updated to '%s'", entry_type,
                        entry_name, zone_name, ip_address)
        else:
            logger.error("error updating '%s':\n%s\n",
                         entry_name, update_response)


def _allowed_gai_family_ipv6():
    if not urllib3_cn.HAS_IPV6:
        logger.error("ERROR: No IPv6 address available!")
        exit(1)
    return socket.AF_INET6


def _allowed_gai_family_ipv4():
    return socket.AF_INET


def fetch_public_ip_address(endpoints):
    ip_address = None
    while ip_address is None:
        for endpoint in endpoints:
            try:
                response = requests.get(endpoint)
                ip_address = response.text.strip()
                _ = ipaddress.ip_address(ip_address)
                break
            except requests.exceptions.ConnectionError as connection_error:
                logger.debug("connection_error: %s", connection_error)
                pass
            except requests.exceptions.ReadTimeout as timeout:
                logger.debug("timeout: %s", timeout)
                pass
            except ValueError as value_error:
                logger.debug("ValueError: %s", value_error)
                ip_address = None
        break
    return ip_address


def get_public_ipv4():
    logger.info("getting public ipv4 address")
    urllib3_cn.allowed_gai_family = _allowed_gai_family_ipv4
    ipv4_endpoints = ["https://api.ipify.org", "https://ipinfo.tw/ip"]
    ipv4_address = fetch_public_ip_address(ipv4_endpoints)
    return ipv4_address


def get_public_ipv6():
    logger.info("getting public ipv6 address")
    urllib3_cn.allowed_gai_family = _allowed_gai_family_ipv6
    ipv6_endpoints = ["https://api6.ipify.org", "https://ipinfo.tw/ip"]
    ipv6_address = fetch_public_ip_address(ipv6_endpoints)
    return ipv6_address


def get_remote_records(existing_remote_records, entry_type, zone_name):
    remote_records = [entry for entry in existing_remote_records
                      if entry.get('type') == entry_type]

    logger.debug("remote_records:\n%s\n", remote_records)
    remote_records_short = [entry.get('name') for entry in remote_records]
    logger.debug("remote_records_short:\n%s\n", remote_records_short)
    remote_records_long = ["%s.%s" % (entry, zone_name)
                           for entry in remote_records_short]
    logger.debug("remote_records_long:\n%s\n", remote_records_long)

    return remote_records


def get_missing_remote_records_short(local_records_short, remote_records_short):
    missing_remote_records_short = []
    for record in local_records_short:
        if not record in remote_records_short:
            missing_remote_records_short.append(record)
    logger.debug("missing_remote_records_short:\n%s\n",
                 missing_remote_records_short)
    return missing_remote_records_short


def get_existing_remote_records(local_records_short, remote_records):
    existing_remote_records = []
    existing_remote_records_short = []
    for local_record_short in local_records_short:
        for remote_record in remote_records:
            if local_record_short == remote_record.get('name'):
                existing_remote_records.append(remote_record)
                existing_remote_records_short.append(
                    remote_record.get('name'))

    logger.debug("existing_remote_records_short:\n%s\n",
                 existing_remote_records_short)
    logger.debug("existing_remote_records:\n%s\n",
                 existing_remote_records)
    return existing_remote_records


def get_filtered_remote_records(all_remote_records, entry_type):
    remote_records = [
        entry for entry in all_remote_records if entry.get('type') == entry_type]
    logger.debug("remote_records:\n%s\n",
                 remote_records)
    return remote_records


def get_filtered_remote_records_short(remote_records):
    remote_records_short = [
        entry.get('name') for entry in remote_records]
    logger.debug("remote_records_short:\n%s\n",
                 remote_records_short)
    return remote_records_short


def main():
    all_local_records_long = read_file_to_list(DNS_ENTRIES_FILENAME)

    # error on invalid entries => no zone match
    for record_long in all_local_records_long:
        no_matching_zone = True
        for zone in dns_zones:
            zone_name = zone.get('name')
            if zone_name in record_long:
                no_matching_zone = False
                break
        if no_matching_zone:
            logger.error(
                "error: no matching dns zone in %s for entry '%s'", SECRETES_YAML_FILE, record_long)

    desired_updates = {}

    if ENABLE_IPV4:
        if MANUAL_IPV4:
            desired_updates["A"] = MANUAL_IPV4
        else:
            desired_updates["A"] = get_public_ipv4()
    if ENABLE_IPV6:
        if MANUAL_IPV6:
            desired_updates["AAAA"] = MANUAL_IPV6
        else:
            desired_updates["AAAA"] = get_public_ipv6()

    # for each zone:
    for zone in dns_zones:
        zone_id, zone_name = get_zone_id_name(zone)
        # collect all local entries in long form, e.g. subdomain.example.org
        local_records_long = [
            item for item in all_local_records_long if item.endswith(zone_name)]
        logger.debug("local_records_long:\n%s\n", local_records_long)

        # trim zone name + dot
        local_records_short = [item[:-len(zone_name)-1]
                               for item in local_records_long]
        logger.debug("local_records_short:\n%s\n", local_records_short)

        # collect all remote entries
        all_remote_records = get_zone_records(zone_id)

        for entry_type in desired_updates:
            entry_content = desired_updates[entry_type]
            # gather relevant entries
            remote_records = get_filtered_remote_records(
                all_remote_records, entry_type)
            remote_records_short = get_filtered_remote_records_short(
                remote_records)

            # which desired entries do already exist?
            existing_remote_records = get_existing_remote_records(
                local_records_short, remote_records)

            # which desired entries are missing?
            missing_remote_records_short = get_missing_remote_records_short(
                local_records_short, remote_records_short)

            # create missing entries
            new_remote_records = create_missing_generic_entries(
                zone_name, zone_id, missing_remote_records_short, entry_content, entry_type)

            # collect old remote entries
            remote_records = get_remote_records(
                existing_remote_records, entry_type, zone_name)

            # update all old remote entries
            update_entries(zone_id, zone_name, remote_records,
                           entry_content, entry_type)

    print("done")


if __name__ == "__main__":
    # execute only if run as a script
    main()
